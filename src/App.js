import React, { useState } from 'react';

const Language = () => {
    const [language, setLanguage] = useState('en');

    const LanguageChange = (lang) => {
        setLanguage(lang);
    };

    return (
        <>
            <div className={'t'}>
                <button onClick={() => LanguageChange('en')}>
                    Кыргызча
                </button>

                <button onClick={() => LanguageChange('fr')}>
                    Русский
                </button>
            </div>
            <div className="r">
            <img src={language === 'en' ? 'https://vlast.kz/media/pages/el/kirgizia_1000x768.jpg' : '/flags'} alt="" className={'a'}/>

            <img src={language === 'fr' ? 'https://filearchive.cnews.ru/img/book/2022/03/04/flag-rossii-02.png' : '/flags'} alt="" className={'im'}/>
            </div>
            <h1>{language === 'en'? 'salam':'privet'}</h1>
        </>
    );
};

export default Language;
